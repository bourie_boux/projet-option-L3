-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 23 Mai 2016 à 10:33
-- Version du serveur :  10.1.13-MariaDB
-- Version de PHP :  7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetm1`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `identifiant` varchar(30) NOT NULL,
  `mdp` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`identifiant`, `mdp`) VALUES
('1', '11'),
('s15023828', 'm1'),
('admin', '1');

-- --------------------------------------------------------

--
-- Structure de la table `choix_etudiant`
--

CREATE TABLE `choix_etudiant` (
  `ine` int(10) NOT NULL,
  `id_option` int(30) NOT NULL,
  `choix_etudiant` int(20) NOT NULL,
  `score` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `etudiants`
--

CREATE TABLE `etudiants` (
  `ine` int(10) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `site` int(10) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `mot_de_passe` varchar(20) NOT NULL,
  `option_valide` int(11) DEFAULT NULL,
  `option_tente_1` int(11) DEFAULT NULL,
  `option_tente_2` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `options`
--

CREATE TABLE `options` (
  `nom` varchar(30) NOT NULL,
  `site` varchar(30) NOT NULL,
  `maximum` int(30) NOT NULL,
  `numero_option` varchar(30) NOT NULL,
  `id_option` int(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `options`
--

INSERT INTO `options` (`nom`, `site`, `maximum`, `numero_option`, `id_option`) VALUES
('AD', 'luminy', 2, '1', 3),
('IA', 'luminy', 7, '1', 2),
('IAA', 'luminy', 4, '1', 1);

-- --------------------------------------------------------

--
-- Structure de la table `options_ouvertes`
--

CREATE TABLE `options_ouvertes` (
  `nom_opt` varchar(60) NOT NULL,
  `site_opt` varchar(30) NOT NULL,
  `max_etu_opt` int(3) NOT NULL,
  `id_option` varchar(20) NOT NULL,
  `dateDebInsc_op` varchar(15) NOT NULL,
  `dateFinInsc_op` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `options_ouvertes`
--

INSERT INTO `options_ouvertes` (`nom_opt`, `site_opt`, `max_etu_opt`, `id_option`, `dateDebInsc_op`, `dateFinInsc_op`) VALUES
('IA', 'luminy,saint charles,aix-en-pr', 5, '2', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `periode`
--

CREATE TABLE `periode` (
  `annee` varchar(30) NOT NULL,
  `formation` varchar(30) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `periode`
--

INSERT INTO `periode` (`annee`, `formation`, `date_debut`, `date_fin`) VALUES
('2016', 'L3', '2016-05-05', '2016-07-03');

-- --------------------------------------------------------

--
-- Structure de la table `resultats`
--

CREATE TABLE `resultats` (
  `id_option` int(30) NOT NULL,
  `ine` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`identifiant`);

--
-- Index pour la table `etudiants`
--
ALTER TABLE `etudiants`
  ADD PRIMARY KEY (`ine`);

--
-- Index pour la table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id_option`);

--
-- Index pour la table `options_ouvertes`
--
ALTER TABLE `options_ouvertes`
  ADD PRIMARY KEY (`id_option`);

--
-- Index pour la table `resultats`
--
ALTER TABLE `resultats`
  ADD PRIMARY KEY (`id_option`,`ine`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `options`
--
ALTER TABLE `options`
  MODIFY `id_option` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
