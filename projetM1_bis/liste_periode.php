<?php
SESSION_START();
if (isset($_SESSION["identifiant"]) && isset($_SESSION["motdepasse"])){

 
?>
<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Gentallela Alela! | </title>

  <!-- Bootstrap core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.3.css" />
  <link href="css/icheck/flat/green.css" rel="stylesheet" />
  <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />


  <link href="js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
  
  <script src="js/jquery.min.js"></script>
  <script src="js/nprogress.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body class="nav-md">
	<div class="container body">
	
		<div class="main_container">
		
			<div class="col-md-3 left_col">

				<div class="left_col scroll-view">

					<div class="navbar nav_title" style="border: 0;">
						<a href="accueil.php" class="site_title"><i class="fa fa-paw"></i> <span>OptionChoice</span></a>
					</div>
					<div class="clearfix"></div>

					  <!-- menu prile quick info -->
					<div class="profile_pic">
						<img src="images/user.png" alt="..." class="img-circle profile_img">
					</div>
					<div class="profile">
						<div class="profile_info">
						  <span>Bienvenue,</span>   
			              <?php
			              echo "<h2>cher(e) : ".$_SESSION["identifiant"]."</h2>";
			              ?>
						</div>
					</div>
					  <!-- /menu prile quick info -->

					<div class="clearfix"></div>

				  <!-- sidebar menu -->
				  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

					<div class="menu_section">
					  <h3>General</h3>
					  <ul class="nav side-menu">
						<li><a href="accueil.php"><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
						</li> 

						<li><a><i class="fa fa-bar-chart-o"></i> Parcours <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu" style="display: none">
								<li><a href="liste-etudiants.php">Liste L3</a>
								</li>
								<li><a href="listeOption.php">Liste Options</a>
								</li>
							</ul>
						</li>

					<li><a><i class="fa fa-clock-o"></i> Période <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu" style="display: none">
                
                <li><a href="liste_periode.php"> Gestion des période</a>
                </li>
              </ul>
            </li>
						
						<li><a><i class="fa fa-file-archive-o"></i> resultat de l'afféctation <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="resultat_tout_affectation.php">tout les étudiants</a></li>
								<li><a>par options<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu" style="display: none">
									<?php
										try
										{
											$bdd = new PDO('mysql:host=localhost;dbname=projetm1','root','');
											$reponse = $bdd->query('select id_option,nom from options');
											while($donne = $reponse->fetch()){
												print('<li><a href=resultat_affectation_par_option.php?id='.$donne['id_option'].'&nom='.$donne['nom'].'>'.$donne['nom'].'</a></li>');
											}
										}
										catch (Exception $e)
										{
											die('Erreur : ' . $e->getMessage());
										}
									?>
									</ul>
								</li>
								<li><a>par site<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu" style="display: none">
									<?php
										try
										{
											$bdd = new PDO('mysql:host=localhost;dbname=projetm1','root','');
											$reponse = $bdd->query('select distinct site from etudiants');
											while($donne = $reponse->fetch()){
												print('<li><a href=resultat_affectation_par_site.php?site='.$donne['site'].'>'.$donne['site'].'</a></li>');
											}
										}
										catch (Exception $e)
										{
											die('Erreur : ' . $e->getMessage());
										}
									?>
									</ul>
								</li>
							</ul>
							
						</li>
						<li><a href='affectation.php'><i class="fa fa-list-ol"></i>requettes des etudiants</a></li>
					  </ul>
					</div>
					
				  </div>
				  <!-- /sidebar menu -->

				</div>
			</div>
			
			<!-- top navigation -->
			<div class="top_nav">

				<div class="nav_menu">
					<nav class="" role="navigation">
						<div class="nav toggle">
						  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
						  <li class="">
							<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							 <?php echo $_SESSION["identifiant"];  ?>
							  <span class=" fa fa-angle-down"></span>
							</a>
							<ul class="dropdown-menu dropdown-usermenu pull-right">
							  <li><?php echo "<a href=\"destroy.php\" ><i class=\"fa fa-sign-out pull-right\"></i> Déconnexion </a>"; ?>
							  </li>
							</ul>
						  </li>
						</ul>
					</nav>
				</div>

			</div>
			<!-- /top navigation -->
			
			<!-- corps -->
			<div class="right_col" role="main">
			
				<div class="page-title">
					<div class="title_left">
						<h3>Liste des Périodes de choix</h3>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
					
						<div class="x_title">
							<ul class="nav navbar-right panel_toolbox">
								<li>
									<a data-toggle="modal" data-target="#ModalInsertion"><i class="fa fa-plus"></i></a>
									<div class="modal fade" id="ModalInsertion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Insertion Période</h4>
												</div>
<div class="modal-body">
<form method="post" action="ins_périod.php?action=insertion" id="form-insertion" data-parsley-validate class="form-horizontal form-label-left">

<div class="form-group">
<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Numero">Anneé<span class="required">*</span>
</label>
<div class="col-md-6 col-sm-6 col-xs-12">
<input type="text" id="Anneé" name="Anneé" required="required" class="form-control col-md-7 col-xs-12">
</div>
</div>
<div class="form-group">
<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Nom">Formation<span class="required">*</span>
</label>
<div class="col-md-6 col-sm-6 col-xs-12">
<input type="text" id="Formation" name="Formation" required="required" class="form-control col-md-7 col-xs-12">
</div>
</div>
<div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Début <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input name="DateDébut" class=" form-control col-md-7 col-xs-12" required="required" type="date">
                      </div>
                    </div>
                    

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Fin <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  name="DateFin" class=" form-control col-md-7 col-xs-12" required="required" type="date">
                      </div>
                    </div>
														 
														  
														
														
														
														
														<div class="ln_solid"></div>
														<div class="form-group">
														  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
															<button type="submit" class="btn btn-success">Submit</button>
															<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														  </div>
														</div>

													</form>
												</div>
											</div>
										</div>
									</div>
								</li>
								
								
							</ul>
							<div class="clearfix"></div>
						</div>
					
						<div class="x_content">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Formation</th>
											<th>Anneé</th>
											<th>Date Début</th>
											<th>Date Fin</th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php
											try
											{
$bdd = new PDO('mysql:host=localhost;dbname=projetm1','root','');
$reponse = $bdd->query('select * from periode');
while($donne = $reponse->fetch()){
													print ("<tr>");
													print('<td>'.$donne['formation'].'</td>');
													print('<td>'.$donne['annee'].'</td>');
													print('<td>'.$donne['date_debut'].'</td>');
													print('<td>'.$donne['date_fin'].'</td>');
													
print ('<td><a data-toggle="modal" href="#modal_modif_'.$donne['annee'].'_'.$donne['formation'].'"><i class="fa fa-pencil"></i></a></td>');
print ('<td><a href="suppression-periode.php?annee='.$donne['annee'].'&formation='.$donne['formation'].'"><i class="fa fa-trash"></i></a></td>');
													print ("</tr>");
											}}
											catch (Exception $e)
											{
												die('Erreur : ' . $e->getMessage());
											}
											?>
									</tbody>
								</table>
									<?php
											try
											{
$bdd = new PDO('mysql:host=localhost;dbname=projetm1','root','');
$reponse = $bdd->query('select * from periode ');
while($donne = $reponse->fetch()){
print ('<div class="modal fade" id="modal_modif_'.$donne['annee'].'_'.$donne['formation'].'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">');
?>
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title">Modification Période du formation <?php print ($donne['formation']) ?> de l'anneé <?php print ($donne['annee']) ?></h4>
</div>
<div class="modal-body">
<form method="post" <?php print ('action="ins_périod.php?action=modification&annee='.$donne['annee'].'&formation='.$donne['formation'].'"'); ?> data-parsley-validate class="form-horizontal form-label-left">

<div class="form-group">
<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Numero">Anneé<span class="required">*</span></label>
<div class="col-md-6 col-sm-6 col-xs-12">
<input type="text"  name="Numero" value=<?php print ('"'.$donne['annee'].'"'); ?> required="required" class="form-control col-md-7 col-xs-12">
																		  </div>
																		</div>
<div class="form-group">
 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Nom">Formation<span class="required">*</span>
</label>
<div class="col-md-6 col-sm-6 col-xs-12">
<input type="text"  name="Nom" value=<?php print ('"'.$donne['formation'].'"'); ?> required="required" class="form-control col-md-7 col-xs-12">
																		  </div>
																		</div>
<div class="form-group">
<label for="Prenom" class="control-label col-md-3 col-sm-3 col-xs-12">Date Début<span class="required">*</span></label>
<div class="col-md-6 col-sm-6 col-xs-12">
<input  type="date" class="form-control col-md-7 col-xs-12" value=<?php print ('"'.$donne['date_debut'].'"'); ?> required="required" type="text" name="date_debut">
																		  </div>
																		</div>
																	
<div class="form-group">
<label for="Site" class="control-label col-md-3 col-sm-3 col-xs-12">Date Fin<span class="required">*</span></label>
<div class="col-md-6 col-sm-6 col-xs-12">
<input type="date" class="form-control col-md-7 col-xs-12" value=<?php print ('"'.$donne['date_fin'].'"'); ?> required="required" type="text" name="date_fin">
																			  </div>
																		</div>
																		

<div class="ln_solid"></div>
<div class="form-group">
<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
<button type="submit" class="btn btn-success">modifier</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																		  </div>
																		</div>

																	</form>
																</div>
															</div>
														</div>
													</div>
											<?php
												}
											}
											catch (Exception $e)
											{
												die('Erreur : ' . $e->getMessage());
											}
											?>
											
						</div>
					</div>
            </div>
			<!-- /corps -->
		</div>
	</div>
	
	<script src="js/bootstrap.min.js"></script>

  <!-- gauge js -->
  <script type="text/javascript" src="js/gauge/gauge.min.js"></script>
  <script type="text/javascript" src="js/gauge/gauge_demo.js"></script>
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
  <!-- chart js -->
  <script src="js/chartjs/chart.min.js"></script>

  <script src="js/custom.js"></script>

    <script>
    NProgress.done();
  </script>
	
	
</body>
<?php
}else {
header("location:login.php?msg=0");
}
?>
</html>
