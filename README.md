# Projet TER Master 1 Choix Options L3


## INSTALLATION 

1. Télécharger le projet et le mettre dans un dossier déstiner à localhost *(htdocs enx lammp)*
2. Créer une base de donnée nommée **projetm1**
3. Importer les tables dans cette base, sur phpadmin, en important le fichier **projetm1.sql**
4. L'installation est fini :)
  * vous pouvez maintenant, allez à la page login.php du projet *(en localhost)*


## UTILISATION
*L'authentification* peut se faire par un administrateur ou un etudiant 

#### 1. Administrateur:

		
  * pour se connécter : 
    * login: admin
    * mot de passe: 1
    * choisir la formation

  * pour consulter la liste des étudiants: aller dans parcours -> Liste L3. Une fois arriver sur la liste des étudiants:
   * ajouter un étudiant:
		* cliquer sur le bouton __+__ 
   * importer des étudiants via un fichier __csv__ :
   		* cliquer sur le bouton attaché(trombone)
   		* le format du fichier csv est le suivant:
    		* __nom,prenom,numero,site,e-mail__
   * pour __supprimer__ un étudiant:
   		* cliquer sur le bouton corbeille
   * pour __modifier__ l'information d'un étudiant:
   		* cliquer sur le bouton crayon
		* ! si l'étudiant est rédoublant, son nom apparaisse en vert et il y a un autre bouton qui apparait pour ajouter ou consulter les matières tentés ou validés par l'étudiant

   * pour consulter la liste des options: aller dans __parcours__ -> __Liste Options__. 
Une fois arriver sur la liste des options:
		* ajouter options:
    		* cliquer sur le bouton __+__ 
		* pour __supprimer__ une option
    		* cliquer sur le bouton corbeille
		* pour __modifier__ une option
    		* cliquer sur le bouton crayon
   * pour __modifier out consulter__ la periode de connexion(pour les étudiants):
	  * aller dans requettes des étudiants
   * pour __consulter__ les voéux des étudiants:
	  * aller dans période -> gestion des périodes
   * pour __lancer l'algorithme__ d'afféctation
	  * aller dans requettes des étudiants-> cliquer sur le bouton "cliquer pour lancer l'afféctation"
   * pour consulter le __resultat__ de l'afféctation
	  * aller dans résultat de l'afféctation

#### 2. Etudiant:
  * pour se connécter:
	  * login: ine de l'étudiant
	  * mot de passe: mot de passe générer dans la table étudiant de la base de donnée
	  * choisir la formation
  * les étudiants peut maintenant faire leur choix dans l'onglet __choix__
  * Ils peuvent aussi modifier leur choix dans l'onglet __mes choix__
